import React, { Component } from 'react';
import { View, Text, Alert } from 'react-native';
import { WebView } from 'react-native-webview';
class PaypalComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    const patchPostMessageFunction = function() {
        var originalPostMessage = window.postMessage;
        var patchedPostMessage = function(message, targetOrigin, transfer) {
            originalPostMessage(message, targetOrigin, transfer);
        };
        patchedPostMessage.toString = function() {
            return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
        };
        window.postMessage = patchedPostMessage;
    };
    this.patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';

  }

  
  handleNavigation(event) {
    'handleNavigation' in this.props && this.props.handleNavigation(event)
}

handleMessage(event) {
    

  let data = event.nativeEvent.data;
  data = JSON.parse(data);

   if (data.status == 'Success') {
       console.log('Payment Success')
    } else {
        console.log('Payment False')
    } 
}
  passValues() {
    let data = {
        amount             : 27,
        orderID            : '153006',
        ProductionClientID : 'AXNxkj3kJQuogrRYFmO3DXmcSBM_ypBTJ1B9ehZPQ5fHQs_956--5zcNzo2frSzlOO75q65Ji1UPi788'
    };
    if (!this.state.sent) {
        this.refs.webview.postMessage(JSON.stringify(data));
        this.setState({
            loading: false,
            sent: true
        });
    }
}

  render() {

    
    return (

          <WebView style = {{overflow: 'scroll'}}
          source={{ uri: 'https://tuweblink.com/diri/paypal2.html' }}
           originWhitelist = {["*"]}
           mixedContentMode = {'always'}
           useWebKit = {Platform.OS == 'ios'}
           onLoadEnd = {() => this.passValues()}

           ref = "webview"
           thirdPartyCookiesEnabled = {true}
           scrollEnabled = {true}
           domStorageEnabled = {true}
           startInLoadingState = {true}
           injectedJavaScript = {this.patchPostMessageJsCode}
           allowUniversalAccessFromFileURLs = {true}
           onMessage = {(event) => this.handleMessage(event)}
            onNavigationStateChange = {(event) => this.handleNavigation(event)}
           javaScriptEnabled = {true}/>
     
    );
  }
}

export default PaypalComponent;
